**Openshift Container Platform v4.3 Disconnected Installation On AWS With A Mirrored Registry**

This repo aims to serve as a guide to perform a OCP4 disconnected installation on AWS user-provided-infrastructure. A disconnected installation ensures the OpenShift Container Platform software is made available to the relevant servers within a high secure environment, then follows the process of a standard installation. This topic additionally details how to manually download the container images and transport them onto the relevant servers.


**THIS REPO IS STILL UNDER CONSTRUCTION => LAST UPDATE PERFORMED ON MONDAY 02-MAR-2020: ONLY STEP 1 (Create local mirrored registry) is tested**

**STEPS:**

**0 - Create local mirrored registry**

![OCP Local Mirrored Registry](media/ocp-local-registry-setup.mp4)

**1 - AWS AIM account for installation**

**2 - Provisioning AWS infrastructure components**

  *2.1 - VPC*

  Cluster Network information

    VPC Cidr: 10.0.0.0/16

    Subnet0: 10.0.0.0/16
    Gateway: 10.0.0.1

    Subnet1: 10.0.1.0/16
    Gateway: 10.0.1.1

    Subnet2: 10.0.2.0/16
    Gateway: 10.0.2.1

    SubnetPublic: 10.0.3.0/16
    Gateway: 10.0.3.1

    Domain: disconnected.lab
    Cluster Name: ocp4poc
    DNS: 10.0.3.10

    api.ocp4poc.disconnected.lab     lb.ocp4poc.disconnected.lab  10.0.3.10
    api-int.ocp4poc.disconnected.lab lb.ocp4poc.disconnected.lab  10.0.3.10
    .apps.ocp4poc.disconnected.lab   lb.ocp4poc.disconnected.lab  10.0.3.10

    registry  registry.disconnected.lab  10.0.3.10
    bastion   bastion.disconnected.lab   10.0.3.10

    bootstrap bootstrap.ocp4poc.disconnected.lab 10.0.0.9

    master-0  master-0.ocp4poc.disconnected.lab  10.0.0.10
    master-1  master-1.ocp4poc.disconnected.lab  10.0.1.10
    master-2  master-2.ocp4poc.disconnected.lab  10.0.2.10

    worker-0 worker-0.ocp4poc.disconnected.lab   10.0.0.20
    worker-1 worker-1.ocp4poc.disconnected.lab   10.0.1.20
    worker-2 worker-2.ocp4poc.disconnected.lab   10.0.2.20

CloudFormation command:

  - ` aws cloudformation create-stack --stack-name vpc --template-body file://templates/2.1.vpc/cluster-vpc-cloudformation-template.yaml --parameters file://templates/2.1.vpc/cluster-vpc-parameters.json `

  *2.2 - Firewalls => Security Groups*

  bastion node : BastionSecurityGroup

  master nodes: ClusterSecurityGroup and MasterSecurityGroup

  worker nodes: ClusterSecurityGroup and WorkerSecurityGroup


  CloudFormation command:

  - ` aws cloudformation create-stack --stack-name securitygroup --template-body file://templates/2.2.securitygroups/cluster-security-groups-cloudformation-template.yaml --parameters file://templates/2.2.securitygroups/cluster-security-groups-parameters.json `

  *2.3 - Provisioning Bootstrap => EC2 instance bootstrap node*

  CloudFormation command:

  - ` aws cloudformation create-stack --stack-name bootstrap --template-body file://templates/2.3.bootstrap/cluster-bootstrap-ec2-instance-cloudformation-template.yaml --parameters file://templates/2.3.bootstrap/cluster-bootstrap-ec2-instance-parameters.json `

  *2.4 - Provisioning Masters => EC2 instance masters nodes*

  CloudFormation command:

  - ` aws cloudformation create-stack --stack-name master --template-body file://templates/2.4.master/cluster-master-ec2-instance-cloudformation-template.yaml --parameters file://templates/2.4.master/cluster-master-ec2-instance-parameters.json `

  *2.5 - Provisioning Workers => EC2 instance workers nodes*

  CloudFormation command:

  - ` aws cloudformation create-stack --stack-name worker --template-body file://templates/2.5.worker/cluster-worker-ec2-instance-cloudformation-template.yaml --parameters file://templates/2.5.worker/cluster-worker-ec2-instance-parameters.json `

**3 - Cluster installtion**

- ` export KUBECONFIG=/root/installation-disconnected/auth/kubeconfig `
- ` oc get nodes `
- ` oc get csr -o name | xargs -n 1 oc adm certificate approve `
- ` openshift-install wait-for bootstrap-complete --dir=. --log-level debug `
- ` oc patch configs.samples.operator.openshift.io/cluster --type merge --patch '{"spec":{"managementState": "Removed"}}' `
- ` openshift-install wait-for install-complete --dir=. --log-level debug `
### "geduld is de moeder van de wetenschap" ###
