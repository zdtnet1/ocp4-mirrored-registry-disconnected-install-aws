#!/bin/bash

source installation-env

cat > external-bastion-mapping.json << EODExternalBastion
[
    {
       "DeviceName" : "/dev/sda1",
       "Ebs": { "VolumeSize" : 50, "DeleteOnTermination": false }
    },
    {
       "DeviceName" : "/dev/sdb",
       "Ebs": { "VolumeSize" : 150, "DeleteOnTermination": false }
    }
]
EODExternalBastion

aws ec2 run-instances --image-id ami-08f4717d06813bf00 --instance-type t2.medium --key-name OcpKeyPair --block-device-mappings file://external-bastion-mapping.json --subnet-id ${PublicSubnetId} --security-group-ids ${BastionSecurityGroupId} --placement AvailabilityZone=${AvailabilityZone0} --private-ip-address 10.0.3.8 > .external-bastion-creation.sh

