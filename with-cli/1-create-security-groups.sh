#!/bin/bash


source installation-env


# 1.0 - Creating BastionSecurityGroup

aws ec2 create-security-group --group-name "BastionSecurityGroup" --description "BastionSecurityGroup" --vpc-id ${VpcId} > .1.0-bastion-security-group-creation.log
echo "export BastionSecurityGroupId=`jq -r .GroupId .1.0-bastion-security-group-creation.log`" >> installation-env

# update env
source installation-env

for PORT in 0
do
    aws ec2 authorize-security-group-ingress --group-id ${BastionSecurityGroupId} --ip-permissions IpProtocol=icmp,FromPort=${PORT},ToPort=${PORT},IpRanges='[{CidrIp=0.0.0.0/0}]'	
done

for PORT in 22 53 80 443 6443 22623 19531 5000 8080
do
    aws ec2 authorize-security-group-ingress --group-id ${BastionSecurityGroupId} --ip-permissions IpProtocol=tcp,FromPort=${PORT},ToPort=${PORT},IpRanges='[{CidrIp=0.0.0.0/0}]'
done

for PORT in 53
do
    aws ec2 authorize-security-group-ingress --group-id ${BastionSecurityGroupId} --ip-permissions IpProtocol=udp,FromPort=${PORT},ToPort=${PORT},IpRanges='[{CidrIp=0.0.0.0/0}]'
done



# 1.1 - Creating ClusterSecurityGroup

aws ec2 create-security-group --group-name "ClusterSecurityGroup" --description "ClusterSecurityGroup" --vpc-id ${VpcId} > .1.1-cluster-security-group-creation.log
echo "export ClusterSecurityGroupId=`jq -r .GroupId .1.1-cluster-security-group-creation.log`" >> installation-env

# update env
source installation-env

for PORT in 0
do
    aws ec2 authorize-security-group-ingress --group-id ${ClusterSecurityGroupId} --ip-permissions IpProtocol=icmp,FromPort=${PORT},ToPort=${PORT},IpRanges='[{CidrIp=10.0.0.0/16}]'
done

for PORT in 22 53 80 443 6443 22623 19531 5000 8080
do
    aws ec2 authorize-security-group-ingress --group-id ${ClusterSecurityGroupId} --ip-permissions IpProtocol=tcp,FromPort=${PORT},ToPort=${PORT},IpRanges='[{CidrIp=10.0.0.0/16}]'
done

for PORT in 53
do
    aws ec2 authorize-security-group-ingress --group-id ${ClusterSecurityGroupId} --ip-permissions IpProtocol=udp,FromPort=${PORT},ToPort=${PORT},IpRanges='[{CidrIp=10.0.0.0/16}]'
done



# 1.2 - Creating MasterSecurityGroup

aws ec2 create-security-group --group-name "MasterSecurityGroup" --description "MasterSecurityGroup" --vpc-id ${VpcId} > .1.2-master-security-group-creation.log
echo "export MasterSecurityGroupId=`jq -r .GroupId .1.2-master-security-group-creation.log`" >> installation-env

# update env
source installation-env

for PORT in 6443 22623
do
    aws ec2 authorize-security-group-ingress --group-id ${MasterSecurityGroupId} --ip-permissions IpProtocol=tcp,FromPort=${PORT},ToPort=${PORT},IpRanges='[{CidrIp=10.0.0.0/16}]'
done



# 1.3 - Creating WorkerSecurityGroup

aws ec2 create-security-group --group-name "WorkerSecurityGroup" --description "WorkerSecurityGroup" --vpc-id ${VpcId} > .1.3-worker-security-group-creation.log
echo "export WorkerSecurityGroupId=`jq -r .GroupId .1.3-worker-security-group-creation.log`" >> installation-env


# updating env
source installation-env


# 1.4 - Master Ingresses Groups

aws ec2 authorize-security-group-ingress --group-id ${MasterSecurityGroupId} --source-group ${MasterSecurityGroupId} --protocol=udp --port=4789

aws ec2 authorize-security-group-ingress --group-id ${MasterSecurityGroupId} --source-group ${WorkerSecurityGroupId} --protocol=udp --port=4789

for PORT in "2379-2380" "9000-9999" "10250-10259" "30000-32767" 
do
    aws ec2 authorize-security-group-ingress --group-id ${MasterSecurityGroupId} --source-group ${MasterSecurityGroupId} --protocol=tcp --port=${PORT}
done

for PORT in "9000-9999" "10250-10259" "30000-32767"
do
    aws ec2 authorize-security-group-ingress --group-id ${MasterSecurityGroupId} --source-group ${WorkerSecurityGroupId} --protocol=tcp --port=${PORT}
done


# 1.5 - Worker Ingresses Groups

aws ec2 authorize-security-group-ingress --group-id ${WorkerSecurityGroupId} --source-group ${WorkerSecurityGroupId} --protocol=udp --port=4789

aws ec2 authorize-security-group-ingress --group-id ${WorkerSecurityGroupId} --source-group ${MasterSecurityGroupId} --protocol=udp --port=4789

for PORT in "9000-9999" "10250" "30000-32767"
do
    aws ec2 authorize-security-group-ingress --group-id ${WorkerSecurityGroupId} --source-group ${WorkerSecurityGroupId} --protocol=tcp --port=${PORT}
done

for PORT in "9000-9999" "10250" "30000-32767"
do
    aws ec2 authorize-security-group-ingress --group-id ${WorkerSecurityGroupId} --source-group ${MasterSecurityGroupId} --protocol=tcp --port=${PORT}
done



# END Security Groups Creation

echo -e "\n\e[1;32m STATUS: SUCCESS => Security Groups Created \e[0m"

