#!/bin/bash

cat > bastion-mapping.json << EODBastion
[
    {
        "DeviceName" : "/dev/sda1",
        "Ebs": { "VolumeSize" : 50, "DeleteOnTermination": false }
    },
    {
        "DeviceName": "/dev/sdb",
        "Ebs": { "VolumeSize" : 100, "DeleteOnTermination": false }
    }
]
EODBastion

aws ec2 run-instances --image-id ami-08f4717d06813bf00 --instance-type t2.medium --key-name OcpKeyPair --block-device-mappings file://bastion-mapping.json --subnet-id ${PrivateSubnet0Id} --security-group-ids ${BastionSecurityGroupId} --placement AvailabilityZone=${AvailabilityZone0} --private-ip-address 10.0.0.5 --associate-public-ip-address > .bastion-creation.log

