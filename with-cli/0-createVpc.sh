#!/bin/bash

source installation-env

# 0.1 - Create VPC

aws ec2 create-vpc --cidr-block ${VpcCidr} > .0.1-vpc-creation.log
echo "export VpcId=`jq -r .Vpc.VpcId .0.1-vpc-creation.log`" >> installation-env

# update env
source installation-env


# 0.2 - Create DHCP Options

aws ec2 create-dhcp-options --dhcp-configuration "Key=domain-name-servers,Values=${DNSServerIp}" "Key=domain-name,Values=${ClusterDomainName}" > .0.2-dhcp-options-creation.log
echo "export DhcpOptionsId=`jq -r .DhcpOptions.DhcpOptionsId .0.2-dhcp-options-creation.log`" >> installation-env

# update env
source installation-env
aws ec2 associate-dhcp-options --dhcp-options-id ${DhcpOptionsId} --vpc-id ${VpcId}


# 0.3 - Create Public Subnet

aws ec2 create-subnet --vpc-id ${VpcId} --cidr-block ${SubnetPublicCidr} --availability-zone-id ${AvailabilityZone0Id} > .0.3-public-subnet-creation.log
echo "export PublicSubnetId=`jq -r .Subnet.SubnetId .0.3-public-subnet-creation.log`" >> installation-env

aws ec2 create-internet-gateway > .0.3-internet-gateway-creation.log
echo "export InternetGatewayId=`jq -r .InternetGateway.InternetGatewayId .0.3-internet-gateway-creation.log`" >> installation-env

aws ec2 allocate-address --domain vpc > .0.3-eip-allocation-creation.log
echo "export EipAllocationId=`jq -r .AllocationId .0.3-eip-allocation-creation.log`" >> installation-env

# update env
source installation-env

aws ec2 create-nat-gateway --subnet-id ${PublicSubnetId} --allocation-id ${EipAllocationId} > .0.3-nat-gateway-creation.log

aws ec2 attach-internet-gateway --vpc-id ${VpcId} --internet-gateway-id ${InternetGatewayId}

aws ec2 create-route-table --vpc-id ${VpcId} > .0.3-route-table-public-subnet-creation.log
echo "export PublicRouteTableId=`jq -r .RouteTable.RouteTableId .0.3-route-table-public-subnet-creation.log`" >> installation-env

# update env
source installation-env

aws ec2 create-route --route-table-id ${PublicRouteTableId} --destination-cidr-block 0.0.0.0/0 --gateway-id ${InternetGatewayId} > .0.3-route-public-subnet-creation.log

aws ec2 associate-route-table --subnet-id ${PublicSubnetId} --route-table-id ${PublicRouteTableId} > .0.3-associate-route-table.creation.log

aws ec2 modify-subnet-attribute --subnet-id ${PublicSubnetId} --map-public-ip-on-launch 



#####################################################################
# subnet 0

aws ec2 create-subnet --vpc-id ${VpcId} --cidr-block ${Subnet0Cidr} --availability-zone-id ${AvailabilityZone0Id} > .0.4-subnet0-creation.log
echo "export PrivateSubnet0Id=`jq -r .Subnet.SubnetId .0.4-subnet0-creation.log`" >> installation-env

aws ec2 create-route-table --vpc-id ${VpcId} > .0.4-route-table-subnet0-creation.log
echo "export PrivateRouteTable0Id=`jq -r .RouteTable.RouteTableId .0.4-route-table-subnet0-creation.log`" >> installation-env

# update env
source installation-env

aws ec2 create-route --route-table-id ${PrivateRouteTable0Id} --destination-cidr-block 0.0.0.0/0 --gateway-id ${InternetGatewayId} > .0.4-route-subnet0-creation.log

aws ec2 associate-route-table --subnet-id ${PrivateSubnet0Id} --route-table-id ${PrivateRouteTable0Id} > .0.4-associate-route-table-subnet0.creation.log


# Temp internet acces
# aws ec2 create-nat-gateway --subnet-id ${PrivateSubnet0Id} --allocation-id ${EipAllocationId}


#####################################################################
# subnet 1

aws ec2 create-subnet --vpc-id ${VpcId} --cidr-block ${Subnet1Cidr} --availability-zone-id ${AvailabilityZone1Id} > .0.4-subnet1-creation.log
echo "export PrivateSubnet1Id=`jq -r .Subnet.SubnetId .0.4-subnet1-creation.log`" >> installation-env

aws ec2 create-route-table --vpc-id ${VpcId} > .0.4-route-table-subnet1-creation.log
echo "export PrivateRouteTable1Id=`jq -r .RouteTable.RouteTableId .0.4-route-table-subnet1-creation.log`" >> installation-env

# update env
source installation-env

aws ec2 create-route --route-table-id ${PrivateRouteTable1Id} --destination-cidr-block 0.0.0.0/0 --gateway-id ${InternetGatewayId} > .0.4-route-subnet1-creation.log

aws ec2 associate-route-table --subnet-id ${PrivateSubnet1Id} --route-table-id ${PrivateRouteTable1Id} > .0.4-associate-route-table-subnet1.creation.log


#####################################################################
# subnet 2

aws ec2 create-subnet --vpc-id ${VpcId} --cidr-block ${Subnet2Cidr} --availability-zone-id ${AvailabilityZone2Id} > .0.4-subnet2-creation.log
echo "export PrivateSubnet2Id=`jq -r .Subnet.SubnetId .0.4-subnet2-creation.log`" >> installation-env

aws ec2 create-route-table --vpc-id ${VpcId} > .0.4-route-table-subnet2-creation.log
echo "export PrivateRouteTable2Id=`jq -r .RouteTable.RouteTableId .0.4-route-table-subnet2-creation.log`" >> installation-env

# update env
source installation-env

aws ec2 create-route --route-table-id ${PrivateRouteTable2Id} --destination-cidr-block 0.0.0.0/0 --gateway-id ${InternetGatewayId} > .0.4-route-subnet2-creation.log

aws ec2 associate-route-table --subnet-id ${PrivateSubnet2Id} --route-table-id ${PrivateRouteTable2Id} > .0.4-associate-route-table-subnet2.creation.log



# END VPC Creation

echo -e "\n\e[1;32m STATUS: SUCCESS => VPC Created \e[0m"
