[
  {
    "ParameterKey": "ClusterName", (1)
    "ParameterValue": "mycluster" (2)
  },
  {
    "ParameterKey": "InfrastructureName", (3)
    "ParameterValue": "mycluster-<random_string>" (4)
  },
  {
    "ParameterKey": "HostedZoneId", (5)
    "ParameterValue": "<random_string>" (6)
  },
  {
    "ParameterKey": "HostedZoneName", (7)
    "ParameterValue": "example.com" (8)
  },
  {
    "ParameterKey": "PublicSubnets", (9)
    "ParameterValue": "subnet-<random_string>" (10)
  },
  {
    "ParameterKey": "PrivateSubnets", (11)
    "ParameterValue": "subnet-<random_string>" (12)
  },
  {
    "ParameterKey": "VpcId", (13)
    "ParameterValue": "vpc-<random_string>" (14)
  }
]


Description:

(1) A short, representative cluster name to use for host names, etc.
(2) Specify the cluster name that you used when you generated the install-config.yaml file for the cluster.
(3) The name for your cluster infrastructure that is encoded in your Ignition config files for the cluster.
(4) Specify the infrastructure name that you extracted from the Ignition config file metadata, which has the format <cluster-name>-<random-string>.
(5) The Route53 public zone ID to register the targets with.
(6) Specify the Route53 public zone ID, which as a format similar to Z21IXYZABCZ2A4. You can obtain this value from the AWS console.
(7) The Route53 zone to register the targets with.
(8) Specify the Route53 base domain that you used when you generated the install-config.yaml file for the cluster. Do not include the trailing period (.) that is displayed in the AWS console.
(9 )The public subnets that you created for your VPC.
(10) Specify the PublicSubnetIds value from the output of the CloudFormation template for the VPC.
(11) The private subnets that you created for your VPC.
(12) Specify the PrivateSubnetIds value from the output of the CloudFormation template for the VPC.
(13) The VPC that you created for the cluster.
(14) Specify the VpcId value from the output of the CloudFormation template for the VPC.
