#!/bin/bash
IFS=","

# Loading the variables
source ocp4_install_env.sh

### NFS Server ###
NFS=true
NFSROOT=/exports
NFS_PROVISIONER=true


#### EXTRA CONFs ###
OCP_REGISTRY_STORAGE_TYPE='nfs'
WEBROOT='/var/www/html'
AIRGAP_REPO='ocp4/openshift4'
AIRGAP_SECRET_JSON='pull-secret.json'


usage() {
    echo " ---- Script Descrtipion ---- "
    echo "  "
    echo " This script configures the bastion host that is meant to serve as local registry and core installation components of Red Hat Openshift 4"
    echo " "
    echo " Pre-requisites: "
    echo " "
    echo " 1 - Update the installation variables in ocp4_install_env.sh"
    echo " 2 - Download the OCP installation secret in https://cloud.redhat.com/openshift/install/pull-secret and create a file called 'redhat-registry-pullsecret.json' in the $HOME directory"
    echo " "

    echo " "
    echo " Options:  "
    echo " "
    echo " * prep_dependencies: installs the os packages needed to perform the registry installation on RHEL8"
    echo " * prep_dns: configures the dns server that will serve the name resolving"
    echo " * prep_loadbalancer: configures the proxy server that will serve the cluster loadbalancing"
    echo " * prep_http: configures the http server that will serve the installation artifacts"
    echo " * prep_nfs: configures the NFS server that will host the registry container images"
    echo " * prep_ignition_files: creates the ignition files for the infrastructre provisioning"
    echo " * get_artifacts: downloads and prepare the oc client and OCP installation program"
    echo " * prep_registry: create and configures the local registry"
    echo " * prep_mirror: mirrors the core registry container images for installation locally"
    echo " * prep_disconnected: performs the whole configuration of the registry in one command"
    echo "  "
    echo -e " Usage: $0 [ prep_dependencies| prep_http | prep_nfs  | prep_ignition_files | get_artifacts | prep_registry | prep_mirror | prep_disconnected ] "
    echo "  "
    echo " ---- Ends Descrtipion ---- "
    echo "  "
}

check_deps (){
    if [[ ! $(rpm -qa wget git bind-utils lvm2 lvm2-libs net-utils firewalld | wc -l) -ge 7 ]] ;
    then
        install_tools
    fi
}

get_artifacts() {
    cd ~/
    test -d artifacts || mkdir artifacts ; cd artifacts
    test -f openshift-client-linux-${OCP_SUBRELEASE}.tar.gz  || curl -J -L -O https://mirror.openshift.com/pub/openshift-v4/clients/${OCP_RELEASE_PATH}/${OCP_SUBRELEASE}/openshift-client-linux-${OCP_SUBRELEASE}.tar.gz
    test -f openshift-install-linux-${OCP_SUBRELEASE}.tar.gz || curl -J -L -O https://mirror.openshift.com/pub/openshift-v4/clients/${OCP_RELEASE_PATH}/${OCP_SUBRELEASE}/openshift-install-linux-${OCP_SUBRELEASE}.tar.gz
    cd ..
    prep_installer
}

prep_http() {
    if [[ $(rpm -qa httpd | wc -l) -ge 1 ]] ;
    then
        if ! grep -q -i "Listen 8080" /etc/httpd/conf/httpd.conf;
            then
                sed -i -e 's/Listen 80/Listen 8080/g' /etc/httpd/conf/httpd.conf
        fi
            firewall-cmd --permanent --add-port=8080/tcp -q
            firewall-cmd --reload -q
            systemctl enable httpd
            systemctl restart httpd
            echo > /var/www/html/index.html
            echo -e "\e[1;32m HTTP - HTTP Server Configuration: DONE \e[0m"
    else
        install_tools
        prep_http
    fi
}

prep_nfs() {
    if [ ${NFS} = true ] ; then
        if [[ $(rpm -qa nfs-utils rpcbind | wc -l) -ge 2 ]] ;
        then
            systemctl enable --now rpcbind
            systemctl enable --now nfs-server
            firewall-cmd --permanent --add-service=mountd -q
            firewall-cmd --permanent --add-service=nfs -q
            firewall-cmd --permanent --add-service=rpc-bind -q
            firewall-cmd --reload -q

            test -d ${NFSROOT} || mkdir -p ${NFSROOT}

            if [ ${NFS_DEV} != false ] ; then
                if [ !  -b /dev/mapper/OCP-nfs ]
                then
                    pvcreate /dev/${NFS_DEV}
                    vgcreate OCP /dev/${NFS_DEV}
                    lvcreate -l 100%FREE -n nfs OCP
                    mkfs.xfs -q /dev/mapper/OCP-nfs
                    #mount /dev/mapper/OCP-nfs ${NFSROOT}
                    if ! grep -qw "/dev/mapper/OCP-nfs" /etc/fstab;
                    then
                        echo "/dev/mapper/OCP-nfs   ${NFSROOT}  xfs defaults    0 0" >> /etc/fstab;
                        mount -a
                    fi
                fi
            fi
            test -d ${NFSROOT}/pv-infra-registry || mkdir ${NFSROOT}/pv-infra-registry
            if [ ${NFS_PROVISIONER} = true ] ; then
                test -d ${NFSROOT}/pv-user-pvs || mkdir ${NFSROOT}/pv-user-pvs
            fi
            cp -rf /etc/exports /etc/exports.$(date "+%Y-%m-%d-%T")
            > /etc/exports
            for NODE in ${MASTERS}; do
                echo "${NFSROOT}/pv-infra-registry $NODE(rw,sync,no_root_squash)" >> /etc/exports
                echo "${NFSROOT}/pv-user-pvs $NODE(rw,sync,no_root_squash)" >> /etc/exports
            done
            for NODE in ${WORKERS}; do
                echo "${NFSROOT}/pv-infra-registry $NODE(rw,sync,no_root_squash)" >> /etc/exports
                echo "${NFSROOT}/pv-user-pvs $NODE(rw,sync,no_root_squash)" >> /etc/exports
            done
            exportfs -a
            test -d ${NFSROOT} && chmod -R 770 ${NFSROOT}
            echo -e "\e[1;32m NFS - NFS Server Configuration: DONE \e[0m"
        else
            install_tools
            prep_nfs
        fi
    fi
}

install_tools() {
    #RHEL8
    if grep -q -i "release 8" /etc/redhat-release; then
        dnf -y install libguestfs-tools podman skopeo httpd haproxy bind bind-utils net-tools nfs-utils rpcbind wget tree git lvm2 lvm2-libs firewalld jq
        echo -e "\e[1;32m Packages - Dependencies installed\e[0m"
    fi

    #RHEL7
    if grep -q -i "release 7" /etc/redhat-release; then
        #subscription-manager repos --enable rhel-7-server-extras-rpms
        yum -y install libguestfs-tools podman skopeo httpd haproxy bind-utils net-tools nfs-utils rpcbind wget tree git lvm2.x86_64 lvm2-libs firewalld bind bind-utils || echo "Please - Enable rhel7-server-extras-rpms repo" && echo -e "\e[1;32m Packages - Dependencies installed\e[0m"
    fi
}

mirror () {
  cd ~/
      echo "Mirroring from Quay into Local Registry"
      test -f /opt/registry/certs/domain.crt && AIRGAP_REG=$(openssl x509 -noout -subject -in /opt/registry/certs/domain.crt | awk '{print $3}') || echo "Local registry not found"
      LOCAL_REGISTRY="${AIRGAP_REG}:5000"
      LOCAL_REPOSITORY="${AIRGAP_REPO}"
      PRODUCT_REPO='openshift-release-dev'
      LOCAL_SECRET_JSON="${AIRGAP_SECRET_JSON}"
      RELEASE_NAME="ocp-release"
      OCP_RELEASE="${RHCOS_IMAGE_BASE}"
      test -f mirror-registry-pullsecret.json || echo "Enter the ADMIN password for local registry:"
      test -f mirror-registry-pullsecret.json || podman login -u admin --authfile mirror-registry-pullsecret.json "${AIRGAP_REG}:5000"

      if [ -f ${RHEL_PULLSECRET} ]
      then
          command -v jq 1>/dev/null 2>/dev/null || { echo >&2 "jq is require but it's not installed.  Aborting."; exit 1; }
          jq -s '{"auths": ( .[0].auths + .[1].auths ) }' mirror-registry-pullsecret.json ${RHEL_PULLSECRET} > ${AIRGAP_SECRET_JSON}

          command -v oc 1>/dev/null 2>/dev/null || { echo >&2 "oc is require but it's not installed.  Aborting."; exit 1; }
          oc adm -a ${LOCAL_SECRET_JSON} release mirror \
          --from=quay.io/${PRODUCT_REPO}/${RELEASE_NAME}:${OCP_RELEASE} \
          --to=${LOCAL_REGISTRY}/${LOCAL_REPOSITORY} \
          --to-release-image=${LOCAL_REGISTRY}/${LOCAL_REPOSITORY}:${OCP_RELEASE}
      else
          echo "ERROR: ${RHEL_PULLSECRET} not found."
      fi
}

prep_ignition_files() {
    cd ~/
    echo "Creating and populating installation folder"
    mkdir ${CLUSTER_NAME}
    cp install-config.yaml ${CLUSTER_NAME}
    echo "Generating ignition files"
    openshift-install create ignition-configs --dir=${CLUSTER_NAME}
    echo "Installing Ignition files into web path"
    test -d ${WEBROOT} || prep_http
    cp -f ${CLUSTER_NAME}/*.ign ${WEBROOT}
    test -d ${WEBROOT} && chmod a+r ${WEBROOT}/*.ign
}

prep_installer () {
    cd ~/
    echo "Uncompressing installer and client binaries"
    test -d ~/bin/ || mkdir ~/bin/
    tar -xzf ./artifacts/openshift-client-linux-${OCP_SUBRELEASE}.tar.gz  -C ~/bin
    tar -xaf ./artifacts/openshift-install-linux-${OCP_SUBRELEASE}.tar.gz -C ~/bin
}

prep_registry (){
  cd ~/
  fqdn=$(dig +short ${AIRGAP_REG})
  if [ ! -z ${fqdn} ]
  then
      test -d /opt/registry/ || mkdir -p /opt/registry/{auth,certs,data}
      if [ ! -f /opt/registry/certs/domain.crt ]
      then
          openssl req -newkey rsa:4096 -nodes -sha256 -keyout /opt/registry/certs/domain.key -x509 -days 365 -subj "/CN=${AIRGAP_REG}" -out /opt/registry/certs/domain.crt
          cp -rf /opt/registry/certs/domain.crt /etc/pki/ca-trust/source/anchors/
          update-ca-trust
          cert_update=true
      fi
      if [ ! -f /opt/registry/auth/htpasswd ]
          then
              echo "Please enter admin user password"
              htpasswd -Bc /opt/registry/auth/htpasswd admin
      fi

  test -f /etc/systemd/system/mirror-registry.service || cat > /etc/systemd/system/mirror-registry.service << EOF
[Unit]
Description=Mirror registry ${AIRGAP_REG}
After=network.target

[Service]
Type=simple
TimeoutStartSec=5m

ExecStartPre=-/usr/bin/podman rm "mirror-registry"
ExecStartPre=/usr/bin/podman pull quay.io/redhat-emea-ssa-team/registry:2
ExecStart=/usr/bin/podman run --name mirror-registry --net host \
-v /opt/registry/data:/var/lib/registry:z \
-v /opt/registry/auth:/auth:z \
-e "REGISTRY_AUTH=htpasswd" \
-e "REGISTRY_AUTH_HTPASSWD_REALM=registry-realm" \
-e "REGISTRY_AUTH_HTPASSWD_PATH=/auth/htpasswd" \
-v /opt/registry/certs:/certs:z \
-e REGISTRY_HTTP_TLS_CERTIFICATE=/certs/domain.crt \
-e REGISTRY_HTTP_TLS_KEY=/certs/domain.key \
quay.io/redhat-emea-ssa-team/registry:2

ExecReload=-/usr/bin/podman stop "mirror-registry"
ExecReload=-/usr/bin/podman rm "mirror-registry"
ExecStop=-/usr/bin/podman stop "mirror-registry"
Restart=always
RestartSec=30

[Install]
WantedBy=multi-user.target
EOF
  /usr/bin/podman pull quay.io/redhat-emea-ssa-team/registry:2 -q 1>/dev/null
  systemctl enable mirror-registry -q
  systemctl start mirror-registry -q
  if [[ ${cert_update} = true ]]
      then
          systemctl restart mirror-registry -q
  fi
  firewall-cmd --permanent --add-port=5000/tcp -q
  firewall-cmd --permanent --add-port=5000/udp -q
  firewall-cmd --reload -q
  echo -e "\e[1;32m Registry - Container Registry Configuration: DONE \e[0m"
else
  echo -e "$AIRGAP_REG \e[1;31m FAIL - DNS Record not found! \e[0m"
fi
}

prep_loadbalancer(){

    cat > /etc/haproxy/haproxy.cfg << EOF
defaults
    mode                    http
    log                     global
    option                  httplog
    option                  dontlognull
    option forwardfor       except 127.0.0.0/8
    option                  redispatch
    retries                 3
    timeout http-request    10s
    timeout queue           1m
    timeout connect         10s
    timeout client          300s
    timeout server          300s
    timeout http-keep-alive 10s
    timeout check           10s
    maxconn                 20000
frontend openshift-api-server
    bind *:6443
    default_backend openshift-api-server
    mode tcp
    option tcplog
frontend machine-config-server
    bind *:22623
    default_backend machine-config-server
    mode tcp
    option tcplog
frontend ingress-http
    bind *:80
    default_backend ingress-http
    mode tcp
    option tcplog
frontend ingress-https
    bind *:443
    default_backend ingress-https
    mode tcp
    option tcplog
backend openshift-api-server
    balance source
    mode tcp
    server bootstrap ${BOOTSTRAP_IP}:6443 check
    server master-0 ${MASTER_0_IP}:6443 check
    server master-1 ${MASTER_1_IP}:6443 check
    server master-2 ${MASTER_2_IP}:6443 check
backend machine-config-server
    balance source
    mode tcp
    server bootstrap ${BOOTSTRAP_IP}:22623 check
    server master-0 ${MASTER_0_IP}:22623 check
    server master-1 ${MASTER_1_IP}:22623 check
    server master-2 ${MASTER_2_IP}:22623 check
backend ingress-http
    balance source
    mode tcp
    server worker-0 ${WORKER_0_IP}:80 check
    server worker-1 ${WORKER_1_IP}:80 check
    server worker-2 ${WORKER_2_IP}:80 check
backend ingress-https
    balance source
    mode tcp
    server worker-0 ${WORKER_0_IP}:443 check
    server worker-1 ${WORKER_1_IP}:443 check
    server worker-2 ${WORKER_2_IP}:443 check
EOF
    setsebool -P haproxy_connect_any 1
    firewall-cmd --permanent --add-service=http -q
    firewall-cmd --permanent --add-service=https -q
    firewall-cmd --permanent --add-port=6443/tcp -q
    firewall-cmd --permanent --add-port=22623/tcp -q
    firewall-cmd --reload -q

    systemctl enable --now haproxy -q
    systemctl restart haproxy -q
    echo -e "\e[1;32m LoadBalancer - HAproxy Configuration: DONE \e[0m"
}

prep_dns(){

  check_deps
  chgrp named -R /var/named
  chown -v root:named /etc/named.conf
  restorecon -rv /var/named
  restorecon /etc/named.conf
  firewall-cmd --permanent --add-port=53/tcp
  firewall-cmd --permanent --add-port=53/udp
  firewall-cmd --permanent --add-port=53/tcp -q
  firewall-cmd --reload -q

  cat > /etc/named.conf << EOF
options {
          listen-on port 53 { 127.0.0.1; ${BASTION_IP}; };
          directory       "/var/named";
          allow-query     { localhost; ${BASTION_IP}/${VPC_NETMASK}; };
          recursion yes;
  };
  zone "forward-${DOMAINNAME}" IN {
           type master;
           file "/var/named/forward-${DOMAINNAME}.db";
           allow-update { none; };
  };
  zone "${MASTER_0_IP_LAST_OCTET}-in-addr.arpa" IN {
            type master;
            file "/var/named/reverse-subnet-0-${DOMAINNAME}.db";
            allow-update { none; };
  };
  zone "${MASTER_1_IP_LAST_OCTET}-in-addr.arpa" IN {
            type master;
            file "/var/named/reverse-subnet-1-${DOMAINNAME}.db";
            allow-update { none; };
  };
  zone "${MASTER_2_IP_LAST_OCTET}-in-addr.arpa" IN {
            type master;
            file "/var/named/reverse-subnet-2-${DOMAINNAME}.db";
            allow-update { none; };
  };
  zone "${MASTER_2_IP_LAST_OCTET}-in-addr.arpa" IN {
            type master;
            file "/var/named/reverse-subnet-3-${DOMAINNAME}.db";
            allow-update { none; };
  };
;
EOF

  cat > /var/named/forward-${DOMAINNAME}.db << EOFA
\$TTL 1W
@   IN  SOA     dns.${DOMAINNAME}. root.${DOMAINNAME}. (
                                                1001    ;Serial
                                                3H      ;Refresh
                                                15M     ;Retry
                                                1W      ;Expire
                                                1D      ;Minimum TTL
                                                )
; Name Server Information
@   IN  NS      dns.${DOMAINNAME}.
;
; IP address of Name Server
@   IN  A       ${BASTION_IP}
;
; Cluster APIs and Wildcard Records (all of them point to the loadbalancer)
api.${CLUSTER_NAME} IN A ${BASTION_IP}
api-int.${CLUSTER_NAME} IN A ${BASTION_IP}
*.apps.${CLUSTER_NAME} IN A ${BASTION_IP}
;
; Create entry for the bootstrap host
bootstrap.${CLUSTER_NAME} IN A ${BOOTSTRAP_IP}
;
; Control Panel Records
master-0.${CLUSTER_NAME} IN A ${MASTER_0_IP}
master-1.${CLUSTER_NAME} IN A ${MASTER_1_IP}
master-2.${CLUSTER_NAME} IN A ${MASTER_2_IP}
;
; Compute Records
worker-0.${CLUSTER_NAME} IN A ${WORKER_0_IP}
worker-1.${CLUSTER_NAME} IN A ${WORKER_0_IP}
worker-2.${CLUSTER_NAME} IN A ${WORKER_0_IP}
;
; ETCD Records
etcd-0.${CLUSTER_NAME} IN A ${MASTER_0_IP}
etcd-1.${CLUSTER_NAME} IN A ${MASTER_1_IP}
etcd-2.${CLUSTER_NAME} IN A ${MASTER_2_IP}
;
; SRV Records
_etcd-server-ssl._tcp.${CLUSTER_NAME} IN SRV 0 10 2380 etcd-0.${CLUSTER_NAME}.${DOMAINNAME}.
_etcd-server-ssl._tcp.${CLUSTER_NAME} IN SRV 0 10 2380 etcd-1.${CLUSTER_NAME}.${DOMAINNAME}.
_etcd-server-ssl._tcp.${CLUSTER_NAME}	IN SRV 0 10 2380 etcd-2.${CLUSTER_NAME}.${DOMAINNAME}.
;
EOFA

 cat > /var/named/reverse-subnet-0-${DOMAINNAME}.db << EOFB
\$TTL 1W
@ IN SOA dns.${DOMAINNAME}. root.${DOMAINNAME}. (
                                                1001    ;Serial
                                                3H      ;Refresh
                                                15M     ;Retry
                                                1W      ;Expire
                                                1D      ;Minimum TTL
                                                )
; Name Server Information
@ IN NS dns.${DOMAINNAME}.
@ IN PTR ${DOMAINNAME}.
;
; Reverse lookup for cluster nodes subnet-0
${MASTER_0_IP_LAST_OCTET} IN PTR master-0.${CLUSTER_NAME}.${DOMAINNAME}.
;
${WORKER_0_IP_LAST_OCTET} IN PTR worker-0.${CLUSTER_NAME}.${DOMAINNAME}.
;
EOFB

 cat > /var/named/reverse-subnet-1-${DOMAINNAME}.db << EOFC
\$TTL 1W
@ IN SOA dns.${DOMAINNAME}. root.${DOMAINNAME}. (
                                               1001    ;Serial
                                               3H      ;Refresh
                                               15M     ;Retry
                                               1W      ;Expire
                                               1D      ;Minimum TTL
                                               )
; Name Server Information
@ IN NS dns.${DOMAINNAME}.
@ IN PTR ${DOMAINNAME}.
;
; Reverse lookup for cluster nodes subnet-1
${MASTER_1_IP_LAST_OCTET} IN PTR master-1.${CLUSTER_NAME}.${DOMAINNAME}.
;
${WORKER_1_IP_LAST_OCTET} IN PTR worker-1.${CLUSTER_NAME}.${DOMAINNAME}.
;
EOFC

 cat > /var/named/reverse-subnet-2-${DOMAINNAME}.db << EOFD
\$TTL 1W
@ IN SOA dns.${DOMAINNAME}. root.${DOMAINNAME}. (
                                               1001    ;Serial
                                               3H      ;Refresh
                                               15M     ;Retry
                                               1W      ;Expire
                                               1D      ;Minimum TTL
                                               )
; Name Server Information
@ IN NS dns.${DOMAINNAME}.
@ IN PTR ${DOMAINNAME}.
;
; Reverse lookup for cluster nodes subnet-2
${MASTER_2_IP_LAST_OCTET} IN PTR master-2.${CLUSTER_NAME}.${DOMAINNAME}.
;
${WORKER_2_IP_LAST_OCTET} IN PTR	worker-2.${CLUSTER_NAME}.${DOMAINNAME}.
;
EOFD

 cat > /var/named/reverse-subnet-3-${DOMAINNAME}.db << EOFE
\$TTL 1W
@ IN SOA dns.${DOMAINNAME}. root.${DOMAINNAME}. (
                                              1001    ;Serial
                                              3H      ;Refresh
                                              15M     ;Retry
                                              1W      ;Expire
                                              1D      ;Minimum TTL
                                              )
; Name Server Information
@ IN NS dns.${DOMAINNAME}.
@ IN PTR ${DOMAINNAME}.
;
; Reverse lookup for cluster nodes subnet-3
${BASTION_IP_LAST_OCTET} IN PTR api.${CLUSTER_NAME}.${DOMAINNAME}.
${BASTION_IP_LAST_OCTET} IN PTR api-int.${CLUSTER_NAME}.${DOMAINNAME}.
;
${BASTION_IP_LAST_OCTET} IN PTR dns.${DOMAINNAME}.
;
EOFE

systemctl enable --now named -q
systemctl restart named -q
echo -e "\e[1;32m DNS => Configuration: DONE \e[0m"

}

prep_disconnected(){
    check_deps
    prep_http
    prep_nfs
    get_artifacts
    prep_installer
    prep_registry
    mirror
}

key="$1"

case $key in
    prep_http)
        prep_http
        ;;
    prep_nfs)
        prep_nfs
        ;;
    get_artifacts)
        get_artifacts
        ;;
    prep_mirror)
        mirror
        ;;
    prep_ignition_files)
        prep_ignition_files
        ;;
    prep_registry)
        prep_registry
        ;;
    prep_disconnected)
        prep_disconnected
        ;;
    prep_dependencies)
        install_tools
        ;;
    prep_loadbalancer)
        prep_loadbalancer
        ;;
    prep_dns)
        prep_dns
        ;;
    *)
        usage
        ;;
esac

##############################################################
# END OF FILE
##############################################################
